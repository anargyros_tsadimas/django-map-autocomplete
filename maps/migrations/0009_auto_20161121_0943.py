# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0008_auto_20161118_1648'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='locality',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
