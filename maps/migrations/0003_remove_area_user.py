# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0002_area'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='area',
            name='user',
        ),
    ]
