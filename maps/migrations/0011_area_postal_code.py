# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0010_area_route'),
    ]

    operations = [
        migrations.AddField(
            model_name='area',
            name='postal_code',
            field=models.CharField(default='', max_length=10),
            preserve_default=False,
        ),
    ]
