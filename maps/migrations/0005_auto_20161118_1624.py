# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0004_area_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='area',
            name='administrative_area_level_1',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='area',
            name='street_number',
            field=models.PositiveSmallIntegerField(null=True, blank=True),
        ),
    ]
