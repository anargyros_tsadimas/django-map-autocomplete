# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0007_auto_20161118_1646'),
    ]

    operations = [
        migrations.RenameField(
            model_name='area',
            old_name='city',
            new_name='country',
        ),
        migrations.RenameField(
            model_name='area',
            old_name='place',
            new_name='locality',
        ),
    ]
