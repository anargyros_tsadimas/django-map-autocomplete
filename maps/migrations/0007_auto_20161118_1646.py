# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0006_auto_20161118_1643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='area',
            name='administrative_area_level_1',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
