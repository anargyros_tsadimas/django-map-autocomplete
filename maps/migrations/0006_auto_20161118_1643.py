# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0005_auto_20161118_1624'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Place',
        ),
        migrations.AlterField(
            model_name='area',
            name='city',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='area',
            name='place',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
