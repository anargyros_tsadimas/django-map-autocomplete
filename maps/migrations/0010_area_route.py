# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0009_auto_20161121_0943'),
    ]

    operations = [
        migrations.AddField(
            model_name='area',
            name='route',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
    ]
