from __future__ import unicode_literals
from django.contrib.auth.models import User


from django.db import models

# Create your models here.

from django.db import models



class Area(models.Model):
    country = models.CharField(max_length=255,blank=True, null=True)
    locality = models.CharField(max_length=255)
    route = models.CharField(max_length=255,blank=True, null=True)
    street_number = models.PositiveSmallIntegerField(blank=True, null=True)
    administrative_area_level_1 = models.CharField(max_length=255,blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)

