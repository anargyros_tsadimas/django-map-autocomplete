from django.shortcuts import render
from django.views.generic import TemplateView, UpdateView, FormView, CreateView, ListView
from .forms import AreaForm
from .models import Area
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse, reverse_lazy

# Create your views here.


class AreaCreateView(FormView):
	form_class = AreaForm
	success_url = reverse_lazy('maps:all')

	def get(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		context = self.get_context_data(**kwargs)
		context['form'] = form
		return self.render_to_response(context)

	def post(self, request, *args, **kwargs):
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		if form.is_valid():
			print '1'
			return self.form_valid(form, **kwargs)
		else:
			print '2'
			return self.form_invalid(form, **kwargs)

	def form_valid(self, form):
		print 'valid'
		locality = form.cleaned_data['locality']
		country = form.cleaned_data['country']
		route = form.cleaned_data['route']
		street_number = form.cleaned_data['street_number']
		administrative_area_level_1 = form.cleaned_data['administrative_area_level_1']
		postal_code = form.cleaned_data['postal_code']
		print locality
		print country
		print route
		print street_number
		print administrative_area_level_1
		print postal_code

		form.save()
		return super(AreaCreateView, self).form_valid(form)

	def form_invalid(self, form, **kwargs):
		print 'invalid'
		context = self.get_context_data(**kwargs)
		context['form'] = form
		return self.render_to_response(context)


class AreaListView(ListView):
	model = Area




