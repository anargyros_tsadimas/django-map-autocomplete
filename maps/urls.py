from django.conf.urls import url
from .views import AreaCreateView, AreaListView

urlpatterns = [

	url(r'^all$', AreaListView.as_view(), name='all'),
	url(r'^add$', AreaCreateView.as_view(template_name='maps/place.html')),
]