
from django import forms
from . models import Area

class AreaForm(forms.ModelForm):
    class Meta:
        model = Area
        fields = ['country', 'locality', 'route', 'street_number', 'administrative_area_level_1', 'postal_code']
